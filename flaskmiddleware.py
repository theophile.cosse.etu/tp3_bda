from flask import Flask
import couchdb_api

app = Flask(__name__)

@app.route("/communes")
def get_all_communes():
    #page = request.args.get('page', default = 1, type = int)
    return couchdb_api.get_all_communes()

@app.route("/communes/<string:code>")
def get_communes_by_code(code):
    return "not yet implemented"
    
@app.route("/departements/<string:code>/communes")
def get_communes_by_departments(code):
    return "not yet implemented"
