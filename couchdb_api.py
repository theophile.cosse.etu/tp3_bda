import requests
from requests.auth import HTTPBasicAuth
import json

COUCHDB_URL = "http://172.28.101.206:5984"

JSON_HEADERS = {"Content-Type": "application/json"}

AUTH_TUPLE = ('cha', 'admin')


def create_db(db_name):
   return requests.put(COUCHDB_URL+"/"+db_name, auth=AUTH_TUPLE).status_code == requests.codes.created

def delete_db(db_name):
   return requests.delete(COUCHDB_URL+"/"+db_name, auth=AUTH_TUPLE).status_code == requests.codes.ok

def add_doc(db_name,doc_name,doc):
    url = COUCHDB_URL+"/"+db_name+"/"+doc_name
    return requests.put(url, data=json.dumps(doc), headers=JSON_HEADERS, auth=AUTH_TUPLE).json().get('rev')

def update_doc(db_name,doc_name,rev,doc):
    url = COUCHDB_URL+"/"+db_name+"/"+doc_name+"?rev="+rev
    return requests.put(url, data=json.dumps(doc), headers=JSON_HEADERS, auth=AUTH_TUPLE).json().get('rev')
    
def delete_doc(db_name,doc_name,rev):
    url = COUCHDB_URL+"/"+db_name+"/"+doc_name+"?rev="+rev
    return requests.delete(url, auth=AUTH_TUPLE).json().get('rev')

def get_doc(db_name,doc_name):
    url = COUCHDB_URL+"/"+db_name+"/"+doc_name
    return requests.get(url, auth=AUTH_TUPLE).json()  

def load_nord_db():
    delete_db("nord")
    create_db("nord")
    with open("nord.json", "r") as read_file:
        nord = json.load(read_file)
        for city in nord:
            add_doc("nord",city["nom"],city)

def load_nord_db():
    delete_db("france")
    create_db("france")
    with open("france.json", "r") as read_file:
        nord = json.load(read_file)
        for city in nord:
            add_doc("france",city["nom"],city)

def create_index(db_name,field):
    url = COUCHDB_URL+"/"+db_name+"/_index"
    data = json.dumps({"index": {"fields": [field]}})
    return requests.post(url,data=data ,auth=AUTH_TUPLE, headers=JSON_HEADERS).json().get('id')

# index code = "_design/9a05fcac4a74b940936946a5303c1e45b5a43de8"


def test_find_doc_with_index(db_name):
    url = COUCHDB_URL+"/"+db_name+"/_find"
    data = json.dumps({
        "selector": {"population": {"$lt": 5000}},
        "execution_stats": True,
        "fields": ["nom", "population"],
        "use_index" : "_design/2f19b9c0b9d038927ddadac01c7f0e507b33bea9"
    })
    return requests.post(url,data=data ,auth=AUTH_TUPLE, headers=JSON_HEADERS).json()

def test_find_doc_without_index():
    url = COUCHDB_URL+"/"+db_name+"/_find"
    data = json.dumps({
        "selector": {"population": {"$lt": 5000}},
        "execution_stats": True,
        "fields": ["nom", "population"]
    })
    return requests.post(url,data=data ,auth=AUTH_TUPLE, headers=JSON_HEADERS).json()


def get_all_communes():
    url = COUCHDB_URL+"/nord/_all_docs"
    return requests.get(url,auth=AUTH_TUPLE).json()

"""
print(delete_db("dbtest"))

print(create_db("dbtest"))
rev = add_doc("dbtest","mondoc",{"wow":3,"ahah":4})
print(rev)
rev = update_doc("dbtest","mondoc",rev,{"wow":3,"ahah":4})
print(rev)

print(get_doc("dbtest","mondoc"))
print(delete_doc("dbtest","mondoc",rev))

print(delete_db("dbtest"))
"""


#download db

open('france.json', 'wb').write(requests.get("https://geo.api.gouv.fr/communes").content)
open('nord.json', 'wb').write(requests.get("https://geo.api.gouv.fr/departements/59/communes").content)


#load_nord_db()

#load_france_db()

#index = create_index("nord","population")
#print(index)
#index code = "_design/9a05fcac4a74b940936946a5303c1e45b5a43de8"


#print(test_find_doc_with_index("nord"))
#print(test_find_doc_without_index("nord"))

#print(test_find_doc_with_index("france"))
#print(test_find_doc_without_index("france"))

