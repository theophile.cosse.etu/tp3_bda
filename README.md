
# Rendu TD3

En raison de la coupure d'electricité le 4/11-5/11 sur le campus ayant durée plus longtemps que prévue (impossible d'acceder aux VM) et la mauvaise gestion de mon temps je n'ai pas terminer les partie pagination et middleware, j'ai donc décris du mieux que j'ai pu comment je les aurais réalisé.

## Indexation
L'indexation ne permet pas de gain de temps significatif sur la base de donnée des villes du Nord.
Je n'ai pas pu tester avec sur la France, on peut s'attendre un gain de temps plutot leger puisque base de donnée ne fait que quelque Mo.

## Pagination
L'interet de la pagination est de pouvoir acceder a une partie des donnée sans avoir a charger leur totalité.
Cette fonctionnalité est indispensable dans les cas ou le contenue d'une table ne peut tenir en ram.
On peut par exemple prendre la liste des des livre de l'API google, il est d'ailleur impossible de recuperer plus de 40 livre à la fois.

[Pagination avec l'api googlebooks](https://developers.google.com/books/docs/v1/using#pagination)

Dans notre cas la pagination peut s'implementer en utilisant le champ limit dans le json envoyer a l'aide de /db/_find.
Il nous suffit ensuite d'envoyer le bookmark recue dans la prochaine requete. [Doc couchdb sur les bookmark](https://docs.couchdb.org/en/latest/api/database/find.html#pagination)

On peut alors creer un [generator en python](https://wiki.python.org/moin/Generators) avec yield ce qui nous donne un itérateur utilisable dans une boucle for in en python.

## Middleware
J'ai choisi d'utiliser serveur web basé sur flask plutot que lighttpd par soucis de simplicité.

### Lancer le serveur flask
```
$ export FLASK_APP=flaskmiddleware
$ flask run
 * Running on http://127.0.0.1:5000/
```

http://127.0.0.1:5000/communes devrais normalement fonctionner
